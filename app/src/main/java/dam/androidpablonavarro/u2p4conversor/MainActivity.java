package dam.androidpablonavarro.u2p4conversor;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.DecimalFormat;

public class MainActivity extends LogActivity {

    private static boolean lastFocus;
    private boolean errorIsVisible;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setUI(){



        final EditText etPulgada = findViewById(R.id.etPulgadas);
        final EditText etResultado = findViewById(R.id.etCentimetros);
        Button buttonConvertir = findViewById(R.id.buttonConvertir);

        final TextView textViewError = findViewById(R.id.textViewError);

        buttonConvertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // TODO: Ex3 Log pulsacion del boton.
                Log.i("LogsConversor", "Se ha pulsado el boton conversor");

                try {

                    // TODO: Ex1 Two-wayconversion inch <-> cm

                    if (lastFocus) {
                        etResultado.setText(convertir(etPulgada.getText().toString()));

                    } else {
                        etPulgada.setText(convertir(etResultado.getText().toString()));
                    }

                    textViewError.setVisibility(View.INVISIBLE);
                    errorIsVisible = false;


                }catch (Exception e) {
                    Log.e("LogsConversor", e.getMessage());

                    //TODO: Ex2 Mensaje de error Sólo números>=1
                    textViewError.setVisibility(View.VISIBLE);
                    errorIsVisible = true;

                }

            }
        });

        // TODO: Ex1 Two-wayconversion inch <-> cm

        etPulgada.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                try {
                    lastFocus = true;
                } catch (Exception e) {
                    Log.e("LogsConversor", e.getMessage());
                }

                return false;
            }
        });

        etResultado.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                try {
                    lastFocus = false;
                } catch (Exception e) {
                    Log.e("LogsConversor", e.getMessage());
                }

                return false;
            }
        });

    }

    // TODO: Ex1 Two-wayconversion inch <-> cm
    private String convertir(String numero) throws Exception {

        DecimalFormat df = new DecimalFormat("0.00");

        double value;

        // TODO: Ex2 Excepcion Sólo números>=1

        if (Double.parseDouble(numero) < 1) {

            throw new Exception("Sólo números>=1");

        }

        if(lastFocus) {

            value =  Double.parseDouble(numero) * 2.54;

        } else {

            value = Double.parseDouble(numero) / 2.54;

        }

        return df.format(value);

    }
    // TODO: Ex4: Preservar estado de la aplicacion. Save y Restore InstanceState.

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        if (errorIsVisible) {

            outState.putBoolean("errorIsVisible", true);

        } else {

            outState.putBoolean("errorIsVisible", false);

        }

    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        final TextView textViewError = findViewById(R.id.textViewError);

        if (savedInstanceState.getBoolean("errorIsVisible")) {

            textViewError.setVisibility(View.VISIBLE);
            errorIsVisible = true;

        } else {

            textViewError.setVisibility(View.INVISIBLE);
            errorIsVisible = false;

        }

    }

}