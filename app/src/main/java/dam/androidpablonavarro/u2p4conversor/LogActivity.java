package dam.androidpablonavarro.u2p4conversor;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

// TODO: Ex3: Clase LogActivity

public class LogActivity extends AppCompatActivity {

    private String DEBUG_TAG = "LOG-" + this.getClass().getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i(DEBUG_TAG, "-----> onCreate() started!!!!");

        //notify("onCreate");

    }



    private void notify(String eventName) {

        String activityName = this.getClass().getSimpleName();

        String CHANNEL_ID = "My_LifeCycle";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, "My Lifecycle", NotificationManager.IMPORTANCE_DEFAULT);

            notificationChannel.setDescription("Lifecycle events");
            notificationChannel.setShowBadge(true);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);

            if(notificationManager != null) {

                notificationManager.createNotificationChannel(notificationChannel);

            }

        }

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(eventName + " " + activityName)
                .setContentText(getPackageName())
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher);

        notificationManagerCompat.notify((int) System.currentTimeMillis(), notificationBuilder.build());

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(isFinishing()) {

            Log.i(DEBUG_TAG, "-----> onDestroy()!!!! (isFinishing() is true) ");

        } else {

            Log.i(DEBUG_TAG, "-----> onDestroy()!!!! (isFinishing() is false) ");

        }

        //notify("onDestroy");

    }

    @Override
    protected void onPause() {
        super.onPause();

        Log.i(DEBUG_TAG, "-----> onPause() !!!!");

        //notify("onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.i(DEBUG_TAG, "-----> onResume() !!!!");

        //notify("onResume");
    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.i(DEBUG_TAG, "-----> onStart() !!!!");

        //notify("onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();

        Log.i(DEBUG_TAG, "-----> onStop() !!!!");

        //notify("onStop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        Log.i(DEBUG_TAG, "-----> onRestart() !!!!");

        //notify("onRestart");
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        Log.i(DEBUG_TAG, "-----> onRestoreInstanceState() !!!!");

    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        Log.i(DEBUG_TAG, "-----> onSaveInstanceState() !!!!");

    }
}
